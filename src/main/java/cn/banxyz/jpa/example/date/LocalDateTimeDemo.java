package cn.banxyz.jpa.example.date;

import org.junit.Test;

import java.time.*;


/**
 * @author ban_xz
 * @date 2020/08/04
 */

public class LocalDateTimeDemo {

    private static final String TEST_PREFIX_DEMO = "THIS_IS_TEST";

    @Test
    public void test1() throws InterruptedException {
        Instant now = Instant.now();
        System.out.println(now);
        Thread.sleep(500l);
        Instant now1 = Instant.now();
        System.out.println(now1);
        long l = Duration.between(now, now1).toMillis();
        System.out.println(l);
        ZonedDateTime zonedDateTime = Instant.now().atZone(ZoneId.systemDefault());
        System.out.println(zonedDateTime);
    }

    @Test
    public void test2(){
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        int year = now.getYear();
        System.out.println(year);
        int dayOfMonth = now.getDayOfMonth();
        System.out.println(dayOfMonth);
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        System.out.println(dayOfWeek);
        Month month = now.getMonth();
        System.out.println(month);
        LocalDateTime plus = now.plus(Duration.ofHours(-4));
        System.out.println(plus);

    }

    @Test
    public void test3(){
        LocalDateTime now = LocalDateTime.now();
        LocalDate localDate = now.toLocalDate();
        LocalTime localTime = now.toLocalTime();
        System.out.println(localDate);
        System.out.println(localTime);

    }





    @Test
    public void test4(){


    }


}
