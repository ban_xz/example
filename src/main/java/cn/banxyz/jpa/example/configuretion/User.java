package cn.banxyz.jpa.example.configuretion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author ban_xz
 * @date 2020/07/07
 */

@Data
@AllArgsConstructor
public class User {

    private String id;
    private String fileName;
    private String message;

}
