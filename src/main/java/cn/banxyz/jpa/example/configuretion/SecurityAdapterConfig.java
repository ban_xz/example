//package cn.banxyz.jpa.example.configuretion;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
///**
// * @author ban_xz
// * @date 2020/07/07
// */
//@Configuration
//public class SecurityAdapterConfig{
//
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    /**
//     * 基于内存的认证方式
//     * @param passwordEncoder
//     * @return
//     */
//    @Bean
//    public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
//        User.UserBuilder users = User.builder().passwordEncoder(passwordEncoder::encode);
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(users.username("1hui").password("123456").roles("guest").build());
//        return manager;
//    }
//
//}
