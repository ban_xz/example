package cn.banxyz.jpa.example;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

/**
 * @author ban_xz
 * @date 2020/07/09
 */

public class InfluxDBDemo {

    public static void insert(int num){
        InfluxDB db = InfluxDBFactory.connect("http://127.0.0.1:8086", "telegraf", "secretpassword");
        db.setDatabase("db0");  // 设置数据库
        Point.Builder builder = Point.measurement("test_measurement");  // 创建Builder，设置表名
        builder.addField("count",num);  // 添加Field
        builder.tag("TAG_CODE","TAG_VALUE_" + num);    // 添加Tag
        Point point = builder.build();
        db.write(point);
    }

}
