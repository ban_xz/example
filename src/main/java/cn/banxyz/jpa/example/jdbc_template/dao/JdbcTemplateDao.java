package cn.banxyz.jpa.example.jdbc_template.dao;

import cn.banxyz.jpa.example.jdbc_template.entity.Province;

import java.util.List;

/**
 * @author ban_xz
 * @date 2020/07/27
 */

public interface JdbcTemplateDao {

    List<Province> findByProdId(Integer provId);
}
