package cn.banxyz.jpa.example.jdbc_template.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author ban_xz
 * @date 2020/07/27
 */
@Data
public class Province {

    private Integer id;

    private Integer provId;

    private String provName;

    private  Integer state;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}
