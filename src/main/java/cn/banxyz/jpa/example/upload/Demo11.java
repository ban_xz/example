package cn.banxyz.jpa.example.upload;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

/**
 * @author ban_xz
 * @date 2020/07/09
 */

public class Demo11 {

    public static void main(String[] args) {
        String filePath = "d://testData.csv";
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
        printMac(filePath, "00-70-A4-00-00-00", 100000);
    }

    private static void printMac(String filePath, String start, int count) {
        start = start.replaceAll("-", "");
        int max=30,min=1;
        String [] items = new String[]{"XIAOMI","HISENSE","TCL","PHILIPS","SUMSUNG","HONOR","LETV","LG","PANDA","CHANGHONG"};
        String [] inin = new String[]{};
        try {
            File file = new File(filePath);
            FileWriter writer = new FileWriter(file, true);
            BigInteger num = new BigInteger(start, 16);
            BigInteger addNum = new BigInteger("1");
            String result = "";
            int province = (int) (Math.random()*(max-min)+min);
            String  vendor = items[new Random().nextInt(items.length)];
            for (int i = 0; i < count; i++) {
                result = num.toString(16).toUpperCase();
                for (int j = 12 - result.length(); j > 0; j--) {
                    result = "0" + result;
                }
                writer.write(result+","+province+","+vendor+"\n");
                num = num.add(addNum);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 组成mac地址
     */
    private static String getMacAdr(String str) {
        StringBuilder result = new StringBuilder("");
        for (int i = 1; i <= 12; i++) {
            result.append(str.charAt(i - 1));
            if (i % 2 == 0) {
                result.append("-");
            }
        }
        return result.substring(0, 17);
    }

}
