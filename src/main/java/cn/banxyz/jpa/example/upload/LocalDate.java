package cn.banxyz.jpa.example.upload;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author ban_xz
 * @date 2020/07/10
 */

public class LocalDate {

    public static void main(String[] args) {
        LocalDateTime startTime = LocalDateTime.of(2020,07,10,13,04);
        LocalDateTime endTime = LocalDateTime.of(2020,07,10,13,05);
        Long start = startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        Long end = endTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        System.out.println(start);
        System.out.println(end);
        System.out.println(start-end);
    }
}
