package cn.banxyz.jpa.example.upload;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author ban_xz
 * @date 2020/07/12
 */

public class CreateMacDemo {

    public static void main(String[] args) throws FileNotFoundException {
        String [] items = new String[]{"XIAOMI","HISENSE","TCL","PHILIPS","SUMSUNG","HONOR","LETV","LG","PANDA","CHANGHONG"};
        List<String> list = new ArrayList<>();
        String start = "00-70-A4-00-00-00";
        start = start.replaceAll("-", "");
        BigInteger num = new BigInteger(start, 16);
        BigInteger addNum = new BigInteger("1");
        String result = "";
        for (int i = 0; i < 10; i++) {
            result = num.toString(16).toUpperCase();
            for (int j = 12 - result.length(); j > 0; j--) {
                result = "0" + result;
            }
            int province = (int) (Math.random()*(810000-110000)+110000);
            String  vendor = items[new Random().nextInt(items.length)];
            String finalData = result +","+ province +","+ vendor;
            num = num.add(addNum);
            list.add(finalData);
        }
        Collections.shuffle(list);
        PrintWriter pw  = new PrintWriter("./nini1.csv");
        list.forEach(pw::println);
        pw.flush();
        pw.close();
    }
}
