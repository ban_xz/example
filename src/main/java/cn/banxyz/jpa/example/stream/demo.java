package cn.banxyz.jpa.example.stream;

import org.thymeleaf.expression.Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ban_xz
 * @date 2020/07/20
 */

public class demo {

    public static void main(String[] args) {
        // 计算空字符串
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        System.out.println("列表: " +strings);
//        long count = getCountEmptyStringUsingJava7(strings);
//        System.out.println("空字符数量为: " + count);
//        long count = strings.stream().filter(string->string.equals("abc")).count();
//        System.out.println("空字符数量为: " + count);



//        List<String> filtered = deleteEmptyStringsUsingJava7(strings);
//        System.out.println("筛选后的列表: " + filtered);
//
//        filtered = strings.stream().filter().collect(Collectors.toList());
//        System.out.println("筛选后的列表: " + filtered);

        String mergedString = getMergedStringUsingJava7(strings,", ");

        System.out.println(mergedString);
        String collect = strings.stream().filter(string ->string.isEmpty()).collect(Collectors.joining("dddd"));

        System.out.println(collect);




    }


    private static String getMergedStringUsingJava7(List<String> strings, String separator){
        StringBuilder stringBuilder = new StringBuilder();

        for(String string: strings){

            if(!string.isEmpty()){
                stringBuilder.append(string);
                stringBuilder.append(separator);
            }
        }
        String mergedString = stringBuilder.toString();
        return mergedString.substring(0, mergedString.length()-2);
    }


    private static List<String> deleteEmptyStringsUsingJava7(List<String> strings){
        List<String> filteredList = new ArrayList<String>();

        for(String string: strings){
            filteredList.add(string);
            if(string.isEmpty()){
                filteredList.add("123");
            }
        }
        return filteredList;
    }

    private static int getCountEmptyStringUsingJava7(List<String> strings){
        int count = 0;
        for(String string: strings){
            if(string.isEmpty()){
                count++;
            }
        }
        return count;
    }
}


