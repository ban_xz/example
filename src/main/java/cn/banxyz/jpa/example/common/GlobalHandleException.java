package cn.banxyz.jpa.example.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author ban_xz
 * @date 2020/07/06
 */
@RestControllerAdvice
public class GlobalHandleException {

    @ExceptionHandler({ServiceException.class})
    public JsonResult<Void> handleException(Throwable e){
        JsonResult<Void> result  = new JsonResult<>(e);
        result.setMessage(e.getMessage());
        if(e instanceof AgeOverScopeException){
            result.setCode(2001);
        }
        return result;
    }

}
