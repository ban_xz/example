package cn.banxyz.jpa.example.common;

import java.io.Serializable;

/**
 * @author ban_xz
 * @date 2020/07/06
 */

public class InsertMIssException extends ServiceException implements Serializable {

    public InsertMIssException() {
        super();
    }

    public InsertMIssException(String message) {
        super(message);
    }

    public InsertMIssException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsertMIssException(Throwable cause) {
        super(cause);
    }

    protected InsertMIssException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
