package cn.banxyz.jpa.example.common;

import java.io.Serializable;

/**
 * @author ban_xz
 * @date 2020/07/06
 */

public class AgeOverScopeException extends ServiceException implements Serializable {

    public AgeOverScopeException() {
        super();
    }

    public AgeOverScopeException(String message) {
        super(message);
    }

    public AgeOverScopeException(String message, Throwable cause) {
        super(message, cause);
    }

    public AgeOverScopeException(Throwable cause) {
        super(cause);
    }

    protected AgeOverScopeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
