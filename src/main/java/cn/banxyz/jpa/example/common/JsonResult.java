package cn.banxyz.jpa.example.common;

import lombok.*;

/**
 * @author ban_xz
 * @date 2020/07/06
 */
@Getter
@Setter
@NoArgsConstructor
public class JsonResult<T> {

    private String message;

    private Integer code;

    private T data;

    public JsonResult(Throwable e) {
        super();
        this.message = e.getMessage();
    }

    public JsonResult(String message, Integer code, T data) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    public JsonResult(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public JsonResult(Integer code) {
        this.code = code;
    }
}
