package cn.banxyz.jpa.example.common;

import java.io.Serializable;

/**
 * @author ban_xz
 * @date 2020/07/06
 */

public class ServiceException extends RuntimeException implements Serializable {

    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    protected ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
