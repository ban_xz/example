package cn.banxyz.jpa.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Random;

//@EnableScheduling
@SpringBootApplication
public class ExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);
        System.out.println("——启动成功——");
    }

    @Scheduled(cron="*/1 * * * * ?")
    public void doInsert(){
        System.out.println("111111111111111111111");
        Random random = new Random();
        InfluxDBDemo.insert(random.nextInt(1000));
    }

}
