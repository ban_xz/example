package cn.banxyz.jpa.example.test;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ban_xz
 * @date 2020/07/14
 */

public class IterableDemo {

    public static void main(String[] args) {

        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        Iterator it = list.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (Integer lists:list
             ) {
            System.out.println(lists);
        }

        list.stream().forEach(System.out::print);



    }


}
