package cn.banxyz.jpa.example.java8.lambda;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ban_xz
 * @date 2020/07/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    private String name;

    private Integer age;

    private Integer salary;

    private Status status;



    public enum Status{
        FREE,
        BUSY,
        VOCATION
    }

}
