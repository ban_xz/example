package cn.banxyz.jpa.example.java8.lambda;

import org.junit.Test;
import org.springframework.data.mongodb.core.mapping.TextScore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author ban_xz
 * @date 2020/07/23
 */

public class Demo1 {

        // 优化方式一 策略设计模式

        //优化方式二

    //--------------------------------------------------------------------
    @Test
    public void test1(){
        List<Enployee> enployee = Arrays.asList(
                new Enployee("张三",15,4000),
                new Enployee("李四",22,3000),
                new Enployee("王五",25,5000),
                new Enployee("赵六",16,6000),
                new Enployee("田七",40,3500),
                new Enployee("花花",42,5000)
        );
        // ----------------------------------------------------------------
        List<Enployee> emps = new ArrayList<>();
        for (Enployee emp:enployee) {
            if (emp.getAge()>22){
                emps.add(emp);
            }
        }
        emps.stream().forEach(System.out::println);

        List<Enployee> list = new ArrayList<>();
        for (Enployee lists:emps) {
            if (lists.getSalary()>4000) {
                list.add(lists);
            }
        }
        list.stream().forEach(System.err::println);
    }



    @Test
    public void test2(){
        List<Enployee> enployee = Arrays.asList(
                new Enployee("张三",15,4000),
                new Enployee("李四",22,3000),
                new Enployee("王五",25,5000),
                new Enployee("赵六",16,6000),
                new Enployee("田七",40,3500),
                new Enployee("花花",42,5000)
        );








        enployee.stream()
                .filter((e) -> e.getSalary() >= 4000)
                .limit(10)
                .forEach(System.out::println);

        enployee.stream()
                .map(Enployee::getName)
                .forEach(System.out::println);




    }
}
