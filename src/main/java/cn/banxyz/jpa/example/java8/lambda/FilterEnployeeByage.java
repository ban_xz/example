package cn.banxyz.jpa.example.java8.lambda;

/**
 * @author ban_xz
 * @date 2020/07/23
 */

public class FilterEnployeeByage implements MyPredicate<Enployee>{


    @Override
    public boolean test(Enployee t) {
        return t.getAge()>20;
    }


}
