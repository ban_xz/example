package cn.banxyz.jpa.example.java8.lambda;

/**
 * @author ban_xz
 * @date 2020/07/23
 */

public interface MyPredicate<T> {

    public boolean test(T t);

}
