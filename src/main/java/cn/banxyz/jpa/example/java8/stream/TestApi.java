package cn.banxyz.jpa.example.java8.stream;

import cn.banxyz.jpa.example.java8.lambda.Employee;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author ban_xz
 * @date 2020/07/24
 */

public class TestApi {

    /**
     查找与匹配
     allMatch  检查是否匹配所有元素
     anyMatch 检查是否至少匹配一个元素
     noneMatch 检查是否没有匹配所有元素
     findFirst 返回第一个元素
     findAny 返回当前流中的任意元素
     count 返回流中的元素总个数
     max 返回流中最大值
     */
     List<Employee> employee = Arrays.asList(
     new Employee("张三",15,4000, Employee.Status.BUSY),
     new Employee("李四",22,3000, Employee.Status.FREE),
     new Employee("王五",25,5000, Employee.Status.VOCATION),
     new Employee("赵六",16,6000, Employee.Status.FREE),
     new Employee("田七",40,3500, Employee.Status.BUSY),
     new Employee("花花",42,5000, Employee.Status.BUSY)
     );

    @Test
    public void test1(){

        boolean b = employee.stream().allMatch((e) -> e.getStatus().equals(Employee.Status.BUSY));
        System.out.println(b);


        boolean b1 = employee.stream().anyMatch((e) -> e.getStatus().equals(Employee.Status.BUSY));
        System.out.println(b1);

        boolean b2 = employee.stream().noneMatch((e) -> e.getStatus().equals(Employee.Status.BUSY));
        System.out.println(b2);

    }

    @Test
    public void test2(){

        Optional<Employee> optional = employee.stream()
                .sorted((e1, e2) -> -Integer.compare(e1.getSalary(), e2.getSalary()))
                .findFirst();
        System.out.println(optional);


        Optional<Employee> max = employee.stream().max((e1, e2) -> -Integer.compare(e1.getSalary(), e2.getSalary()));
        System.out.println(max);
    }


}
