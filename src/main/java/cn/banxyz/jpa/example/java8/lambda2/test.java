package cn.banxyz.jpa.example.java8.lambda2;

/**
 * @author ban_xz
 * @date 2020/07/23
 */

import org.junit.Test;

import java.util.Comparator;
import java.util.function.Consumer;

/**
 * Lambda 基础语法："->"该操作为 箭头操作 或 lambda操作符
 *
 * 左侧：Lambda表达式的参数列表
 * 右侧：lambda表达式中所需执行的功能，即lambda体
 *
 *  语法格式一：无参数、无返回值
 *     () -> System.out.println("123");
 *
 * 语法格式二：若有一个参数，小括号可以省略不写
 *     (e) -> System.out.println(e);
 *
 * 语法格式三： 若有一个参数，小括号可以省略不写
 *     e -> System.out.println(e);
 *
 * 语法格式四：若有两个以上参数，有返回值并且lambda体中有多条语句
 *             Comparator<Integer> con = (x, y) -> {
 *                   System.out.println("lambda");
 *                   return Integer.compare(x, y);
 *            };
 *
 * 语法格式五：若lambda体中只有一条语句，{}和return都可以不写
 *    Comparator<Integer> com = (x, y) ->Integer.compare(x, y);
 *
 * 语法格式六： Lambda表达式的参数列表的数据类型可以省略不写，因为编译器可以通过上下文推断
 *     Comparator<Integer> com = (Integer x,Integer y) ->Integer.compare(x, y);
 *
 * 上联：左右遇一括号省
 * 下联：左侧推断类型省
 *  能省则省
 *
 */
public class test {
    int num = 1;

    /**
     * 语法格式一：无参数、无返回值
     *  () -> System.out.println("123");
     */
    // jdk 1.7 以前 匿名内部类中使用局部变量 必须是fina类型的
    @Test
    public void test1(){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println("123"+ num++);
            }
        };
        r.run();
        // -------------------------------------------------------
        Runnable r1 = () -> System.out.println("456");
        r1.run();
    }

    /**
     * 语法格式二：有一个参数无返回值
     */
    @Test
    public void test2(){
        Consumer<String> con = (e) -> System.out.println(e);
        con.accept("aaaaa");
    }

    /**
     * 语法格式三：若有一个参数，小括号可以省略不写
     */
    @Test
    public void test3(){
        Consumer<String> con = e -> System.out.println(e);
        con.accept("aaaaa");
    }

    /**
     * 语法格式四：若有两个以上参数，有返回值并且lambda体中有多条语句
     *             Comparator<Integer> con = (x, y) -> {
     *                   System.out.println("lambda");
     *                   return Integer.compare(x, y);
     *            };
     */
    @Test
    public void test4(){
        Comparator<Integer> con = (x, y) -> {
          System.out.println("lambda");
            return Integer.compare(x, y);
        };

    }


    @Test
    public void test5(){
        Comparator<Integer> com = (x, y) ->Integer.compare(x, y);

    }

    /**
     *语法格式六： Lambda表达式的参数列表的数据类型可以省略不写，因为编译器可以通过上下文推断
     */
    @Test
    public void test6(){
        Comparator<Integer> com = (Integer x,Integer y) ->Integer.compare(x, y);

    }



}
