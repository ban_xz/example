package cn.banxyz.jpa.example.java8.stream;

import cn.banxyz.jpa.example.java8.lambda.Employee;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ban_xz
 * @date 2020/07/24
 */

public class Demo2 {
    List<Employee> employee = Arrays.asList(
            new Employee("张三",15,4000, Employee.Status.BUSY),
            new Employee("李四",22,3000, Employee.Status.FREE),
            new Employee("王五",25,5000, Employee.Status.VOCATION),
            new Employee("赵六",16,6000, Employee.Status.FREE),
            new Employee("田七",40,3500, Employee.Status.BUSY),
            new Employee("花花",42,5000, Employee.Status.BUSY)
    );

    @Test
    public void test1(){

        List<Integer> list = Arrays.asList(1,2,3,4,5);
        List<Integer> collect = list.stream().map((e) -> e * e).collect(Collectors.toList());
        System.out.println(collect);

    }

    @Test
    public void test2(){

        Optional<Integer> reduce = employee.stream()
                .map((e) -> 1)
                .reduce(Integer::sum);
        System.out.println(reduce.get());

    }
}
