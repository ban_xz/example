package cn.banxyz.jpa.example.java8.stream;

import cn.banxyz.jpa.example.java8.lambda.Employee;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ban_xz
 * @date 2020/07/24
 */

public class TestApi2 {

    List<Employee> employee = Arrays.asList(
            new Employee("张三",15,4000, Employee.Status.BUSY),
            new Employee("李四",22,3000, Employee.Status.FREE),
            new Employee("王五",25,5000, Employee.Status.VOCATION),
            new Employee("赵六",16,6000, Employee.Status.FREE),
            new Employee("田七",40,3500, Employee.Status.BUSY),
            new Employee("花花",42,5000, Employee.Status.BUSY)
    );

    /**
     * 收集  collect将流转换为其他形式，接受一个Collector接口的实现，用于给Stream中元素做汇总
     */
    @Test
    public void test1(){
        List<Integer> list  = Arrays.asList(1,2,3,4,5,6,7);

        Integer reduce = list.stream().reduce(0, (x, y) -> x + y);
        System.out.println(reduce);

        Integer reduce1 = employee.stream()
                .map(Employee::getSalary)
                .reduce(0, (x, y) -> x + y);
        System.out.println(reduce1);

        List<String> collect = employee.stream()
                .map(Employee::getName)
                .collect(Collectors.toList());
        System.out.println(collect);

    }

    @Test
    public void test2(){
        // 总数
        Long collect = employee.stream()
                .collect(Collectors.counting());
        System.out.println(collect);

        //平均值
        Double collect1 = employee.stream()
                .collect(Collectors.averagingInt(Employee::getSalary));
        System.out.println(collect1);

        //求和
        Integer collect2 = employee.stream()
                .collect(Collectors.summingInt(Employee::getSalary));
        System.out.println(collect2);

        //最大值
        Optional<Employee> collect3 = employee.stream()
                .collect(Collectors.maxBy((e1, e2) -> Integer.compare(e1.getSalary(), e2.getSalary())));
        System.out.println(collect3);

        //最小值
        Optional<Integer> collect4 = employee.stream()
                .map(Employee::getSalary)
                .collect(Collectors.minBy(Integer::compare));
        System.out.println(collect4);


    }
    @Test
    public void test3(){
        Map<Employee.Status, List<Employee>> collect = employee.stream()
                .collect(Collectors.groupingBy(Employee::getStatus));
        System.out.println(collect);

    }

    @Test
    public void test4(){
        // 多级分组
        Map<Employee.Status, Map<String, List<Employee>>> collect = employee.stream()
                .collect(Collectors.groupingBy(Employee::getStatus, Collectors.groupingBy((e) -> {
                    if ((e).getAge() <= 35) {
                        return "青年";
                    } else if ((e).getAge() <= 50) {
                        return "中年";

                    } else {
                        return "老年";
                    }
                })));
        System.out.println(collect);

    }

    @Test
    public void test5(){

        Map<Boolean, List<Employee>> collect = employee.stream()
                .collect(Collectors.partitioningBy((e) -> e.getSalary() >= 6000));
        System.out.println(collect);

    }

    @Test
    public void test6(){
        IntSummaryStatistics collect = employee.stream()
                .collect(Collectors.summarizingInt(Employee::getSalary));
        System.out.println(collect.getSum());
        System.out.println(collect.getAverage());
        System.out.println(collect.getMax());
        

    }
    
    @Test
    public void test8(){
        String collect = employee.stream()
                .map(Employee::getName)
                .collect(Collectors.joining(",","首","未"));
        System.out.println(collect);

    }
}
