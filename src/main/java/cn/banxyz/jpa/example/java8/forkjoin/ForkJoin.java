package cn.banxyz.jpa.example.java8.forkjoin;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.RecursiveTask;

/**
 * @author ban_xz
 * @date 2020/07/25
 */

public class ForkJoin extends RecursiveTask<Long> implements Serializable {

    private long start;
    private long end;

    private static final long THRESHOLD = 1000;

    @Override
    protected Long compute() {
        Instant now = Instant.now();
        Instant now1 = Instant.now();


        long length = end -start;
        if(length <= THRESHOLD){
            long sum = 0;
            for (long i = start; i <= end ; i++) {
                sum += i;
            }
        }



        return null;
    }
}
