package cn.banxyz.jpa.example.java8.optional;

import cn.banxyz.jpa.example.java8.lambda.Employee;
import org.junit.Test;

import java.util.Optional;

/**
 * @author ban_xz
 * @date 2020/07/25
 */

public class OptionalTest {

    @Test
    public void test4(){

        Optional<Employee> op = Optional
                .ofNullable(new Employee("张三", 18, 2000, Employee.Status.BUSY));
//        Optional<String> s = op.map((e) -> e.getName());
//        System.out.println(s);

        Optional<String> s = op.flatMap((e) -> Optional.of(e.getName()));
        System.out.println(s);

    }


    @Test
    public void test2(){
        Optional<Employee> employee = Optional.ofNullable(null);

//        if (employee.isPresent()){
//            System.out.println(employee.get());
//        }

//        Employee op = employee.orElse(new Employee("张三", 18, 2000, Employee.Status.BUSY));
//        System.out.println(op);

        Employee employee1 = employee.orElseGet(() -> new Employee());
        System.out.println(employee1);
    }

    @Test
    public void test3(){
        Optional<Employee> empty = Optional.empty();
        System.out.println(empty.get());

    }


    @Test
    public void test1(){
        Optional<Employee> employee = Optional.of(new Employee());

        Employee employee1 = employee.get();
        System.out.println(employee1);

    }

}
