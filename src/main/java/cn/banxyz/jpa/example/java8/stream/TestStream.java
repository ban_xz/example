package cn.banxyz.jpa.example.java8.stream;

import cn.banxyz.jpa.example.java8.lambda.Enployee;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author ban_xz
 * @date 2020/07/24
 */

public class TestStream {

    List<Enployee> enployee = Arrays.asList(
            new Enployee("张三",15,4000),
            new Enployee("李四",22,3000),
            new Enployee("王五",25,5000),
            new Enployee("赵六",16,6000),
            new Enployee("田七",40,3500),
            new Enployee("田七",40,3500),
            new Enployee("田七",40,3500),
            new Enployee("花花",42,5000)
    );




    /**
     * 自然排序
     */
    @Test
    public void test8(){
        List<String> list = Arrays.asList("ddd","222","aaa", "bbb", "ccc","111");
        list.stream().sorted().forEach(System.out::println);

        enployee.stream().sorted((e1,e2) -> {
            if (e1.getAge().equals(e2.getAge())){
                return e1.getName().compareTo(e2.getName());
            }else {
                return -e1.getAge().compareTo(e2.getAge());
            }
        }).forEach(System.out::println);

    }


    @Test
    public void test7(){
        List<String> list = Arrays.asList("aaa", "bbb", "ccc", "ddd");
        List<String> list2 = new ArrayList<>();
        list2.add("111");
        list2.addAll(list);
        System.out.println(list2);
    }

    /**
     * map 接受Lambda将元素转换成形式或提取信息，接受一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素。
     * filatMap 接受一个函数作为参数，将流中的每个值都换成一个流，然后把所有流连接成一个流
     */
    @Test
    public void test6(){
        List<String> list = Arrays.asList("aaa", "bbb", "ccc", "ddd");
        list.stream().map((e) -> e.toUpperCase()).forEach(System.out::println);

        Stream<String> s1 = enployee.stream().map(Enployee::getName);
    }

//    @Test
//    public static Stream<Character> test7(String str){
//        List<Character> characters = new ArrayList<>();
//
//        for (Character ch:str.toCharArray()) {
//            characters.add(ch);
//        }
//        return characters.stream();
//    }


    // skip 跳过元素，返回一个扔掉了前n个元素的流，若流中元素不足n个则返回一个空流，与limit互补
    @Test
    public void test4(){
        enployee.stream().filter((e)-> e.getAge()>20).distinct().forEach(System.out::println);
    }


    @Test
    public void test3(){
        enployee.stream().filter((e) -> {
                 System.out.println("短路");
                 return  e.getSalary()>=4000;
             })
             .limit(2)
             .forEach(System.out::println);
    }


    @Test
    public void test1(){
    // 内部迭代
        Stream<Enployee> enployeeStream = enployee.stream().filter((e) ->{
                 System.out.println("111111111111");
                return  e.getAge() > 20;
        });
    // Stream<Enployee> enployeeStream = enployee.stream().filter((e) -> e.getAge() > 20);
    // 终止操作:"惰性求值"
        enployeeStream.forEach(System.out::println);

    }

    @Test
    public void test2(){
        // 外部迭代
        Iterator<Enployee> iterator = enployee.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }


}
