package cn.banxyz.jpa.example.java8.example;

import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * @author ban_xz
 * @date 2020/07/24
 */

public class test {

    List<Transaction> transactions = null;

    @Before
    public void before(){
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
    }

    @Test
    public void test1(){
        List<Transaction> collect = transactions.stream()
                .filter((e) -> e.getYear() == 2012)
                .sorted((t1, t2) -> -Integer.compare(t1.getValue(), t2.getValue()))
                .collect(Collectors.toList());
        collect.forEach(System.out::println);

    }
        @Test
        public void test2(){
            transactions.stream()
                    .map((e) -> e.getTrader().getCity())
                    .distinct()
                    .forEach(System.out::println);
        }


        @Test
        public void test3(){
        transactions.stream()
                .filter((e) -> e.getTrader().getCity().equals("Cambridge"))
                .map(Transaction::getTrader)
                .sorted((e1,e2)-> e1.getName().compareTo(e2.getName())).forEach(System.out::println);

        }


        @Test
        public void test4(){

            String collect = transactions.stream()
                    .map((e) -> e.getTrader().getName())
                    .sorted()
                    .collect(Collectors.joining("\n"));
            System.out.println(collect);

        }

        @Test
        public void test5(){
            String str = "周";
            char[] chars = str.toCharArray();
            System.out.println((int)chars[0]);

            int a = (int)chars[0];
            System.out.println((char) a);

            boolean miLan = transactions.stream()
                    .anyMatch((e) -> e.getTrader().getCity().equals("MiLan"));
            System.out.println(miLan);


            Optional<Integer> cambridge = transactions.stream()
                    .filter((e) -> e.getTrader().getCity().equals("Cambridge"))
                    .map(Transaction::getValue)
                    .reduce(Integer::sum);
            System.out.println(cambridge);
        }


        @Test
        public void test17(){
            Optional<Integer> max = transactions.stream()
                    .map((e) -> e.getValue())
                    .max(Integer::compare);
            System.out.println(max);


            Optional<Integer> min = transactions.stream()
                    .map((e) -> e.getValue())
                    .min(Integer::compareTo);
            System.out.println(min);
        }


    @Test
    public void test8(){
        Instant now = Instant.now();
        LongStream.rangeClosed(0, 10000000000L)
                .parallel()
                .reduce(0,Long::sum);
        Instant now1 = Instant.now();
        System.out.println(Duration.between(now1, now).toMillis());

    }






}
