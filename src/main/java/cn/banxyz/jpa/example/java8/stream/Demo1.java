package cn.banxyz.jpa.example.java8.stream;

import cn.banxyz.jpa.example.demo.dao.CustomerDao;
import cn.banxyz.jpa.example.demo.entity.Custer;
import cn.banxyz.jpa.example.java8.lambda.Enployee;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * 数据源： 集合、数组等
 *
 * 数据源 --> 转化为流 --> 对数据操作 --> 新流
 *
 *
 */

public class Demo1 {


    @Test
    public void test1(){
        // Collection提供的Stream() 或 parallelStream()
        List<String> list = new ArrayList<>();
        Stream<String> stream = list.stream();


        //通过Arrays中的静态方法
        Enployee[] enployees =  new Enployee[10];
        Stream<Enployee> stream1 = Arrays.stream(enployees);


        //通过Stream 类中的静态方法 of()
        Stream<String> aa = Stream.of("aa", "bb");


        // 创建无限流 迭代
        Stream<Integer> iterate = Stream.iterate(0, (e) -> e + 1);
//        iterate.limit(10).forEach(System.out::println);

        //生成
        Stream<Double> generate = Stream.generate(() -> Math.random());
        generate.limit(10).forEach(System.out::println);
    }
    
}
