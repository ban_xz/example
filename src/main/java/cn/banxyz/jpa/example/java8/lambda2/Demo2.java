package cn.banxyz.jpa.example.java8.lambda2;

import cn.banxyz.jpa.example.java8.lambda.Enployee;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @author ban_xz
 * @date 2020/07/23
 */

public class Demo2 {

    /**
     *    Predicate<T> ：断言接口
     */

    @Test
    public void tets4(){
        List<String> list = Arrays.asList("hello","aaa","eeee");
        List<String> list1 = filterStr(list, (e) -> e.length() > 3);
        list1.stream().forEach(System.out::println);
    }

    public List<String> filterStr(List<String> str, Predicate<String> predicate){
        List<String> list = new ArrayList<>();
        for (String lists:str) {
            if (predicate.test(lists)){
                list.add(lists);
            }
        }
        return list;
    }



    /**
     *  Function<T,R> :函数型接口
     */
    @Test
    public void tets3(){
        String newStr = strHandler("张三ddeeaa", (e) -> e.toUpperCase());
        System.out.println(newStr);
    }

    public String strHandler(String str, Function<String,String> function){
        return function.apply(str);
    }



    /**
     *  Supplier<T>  供给型接口
     */

    @Test
    public void tets2(){
        List<Integer> numberList = getNumberList(10, () -> (int)(Math.random() * 100));
        numberList.stream().forEach(System.out::println);
    }

    public List<Integer> getNumberList(int num, Supplier<Integer> supplier){
        List<Integer> objects = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            Integer n = supplier.get();
            objects.add(n);
        }
        return objects;
    }



    /**
     *    Consumer<T>  : 消费型接口
     */
    @Test
    public void tets1(){
        happy(1000,(e) -> System.out.println(e+"元"));
    }

    public void happy(double money, Consumer<Double> con){
        con.accept(money);
    }

}
