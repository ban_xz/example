package cn.banxyz.jpa.example.java8.lambda2;


import cn.banxyz.jpa.example.java8.lambda.Enployee;
import org.junit.Test;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 方法引用：若Lambda 体中的内容有方法已经实现了，我们可以使用"方法引用"
 * （方法引用就是Lambda表达式的另一种表现形式）
 *
 * 对象：：实例方法名
 *
 * 类：：静态方法名
 *
 * 类：：实例方法名
 *
 *  注意：
 *  1.Lambda体中调用方法的参数列表与返回值类型，要与函数式接口中抽象方法的函数列表和返回值类型保持一致。
 *  2.若lambda参数列表中的第一参数是实例方法的调用者，而第二参数是实例方法的参数时，可以是使用 ClassName：：method
 *
 */
public class TestMethodRef {

    @Test
    public  void test1(){
        Consumer<String> con = System.out::println;
        con.accept("aaa");
    }

    @Test
    public  void test2(){
        Enployee enployee = new Enployee();
        Supplier<String> sup = enployee::getName;
        String name = sup.get();
        System.out.println(name);

    }

    @Test
    public  void test3(){
        Comparator<Integer> com = Integer::compare;

    }

    @Test
    public void test4(){
        Supplier<Enployee> supplier = () -> new Enployee();
        Supplier<Enployee> supplier1 = Enployee::new;
        Enployee enployee = supplier1.get();
        System.out.println(enployee);
    }


    @Test
    public void test5(){

        Function<Integer,String[]> fun = (e) -> new String[e];
        String[] str = fun.apply(2);
        System.out.println(str);

        Function<Integer,String[]> funs = String[]::new;
        String[] strs = funs.apply(2);
        System.out.println(strs);

    }
}
