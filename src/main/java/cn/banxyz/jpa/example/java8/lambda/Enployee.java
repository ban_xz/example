package cn.banxyz.jpa.example.java8.lambda;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * @author ban_xz
 * @date 2020/07/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Enployee {

    private String name;

    private Integer age;

    private Integer salary;



}
