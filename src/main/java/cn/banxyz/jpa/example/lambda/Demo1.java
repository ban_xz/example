package cn.banxyz.jpa.example.lambda;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ban_xz
 * @date 2020/07/20
 */

public class Demo1 {

    public static void main(String[] args) {
        System.out.println("");
        List<String> list = Arrays.asList("111","22","","333","44","222","","22");
        System.out.println(list);

//        List<String> qq = list.stream().filter(lists -> lists.matches("111")).collect(Collectors.toList());
//        System.out.println(qq);
//
//        long count = list.stream().filter(string -> string.equals("222")).count();
//        System.out.println(count);
//
//        Stream<String> stream = Stream.of("张三","李四","王麻子","张六","王八");
//        System.out.println(stream);
//        Stream<String> ll = stream.filter(name -> name.startsWith("张"));
//        ll.forEach(name-> System.out.println(name));


        List<String> collect = list.stream()
                .filter(name -> name.length() == 3)
                .peek(System.out::println)
                .collect(Collectors.toList());

        List<Map<String,Object>> lists=new ArrayList<>();
        for(int i=0;i<5;i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("type", i);
            lists.add(map);
        }

        System.out.println(lists);

        List<Map<String, Object>> type = lists.stream().filter((Map a) -> ("2".equals(a.get("type").toString()))).collect(Collectors.toList());
        System.out.println(type);




    }

}
