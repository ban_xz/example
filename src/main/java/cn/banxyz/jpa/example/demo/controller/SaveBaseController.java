package cn.banxyz.jpa.example.demo.controller;

import cn.banxyz.jpa.example.common.JsonResult;
import cn.banxyz.jpa.example.demo.entity.User;
import cn.banxyz.jpa.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * @author ban_xz
 * @date 2020/07/06
 */

@RestController
@RequestMapping("/insert")
public class SaveBaseController {

    @Autowired
    UserService userService;

    @RequestMapping("/info")
    public JsonResult<Void> Insert(@RequestBody User json){
        System.out.println("User="+json);
        userService.insertInfo(json);
        return new JsonResult("成功",200);
    }

}
