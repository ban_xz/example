package cn.banxyz.jpa.example.demo.dao;

import cn.banxyz.jpa.example.demo.entity.Custer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.w3c.dom.ls.LSInput;

import java.util.List;


/**
 * @author ban_xz
 * @date 2020/07/16
 */

public interface CustomerDao extends JpaRepository<Custer,Long>, JpaSpecificationExecutor<Custer> {

    @Query(value = "from Custer where custId = :custId and custName = :custName   ")
    public Custer findJpal(String custName,Long custId);

    @Query(value = "update Custer set custName = :custName where custId = :custId")
    @Modifying
    public void updateCustomer(long custId,String custName);

    @Query(value = "select *from cst_customer where cust_name like ?1",nativeQuery = true)
    public List<Object[]> findMySql(String custName);

    List<Custer> findByCustNameAndAndCustId(String custName,Long id);


}
