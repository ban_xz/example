package cn.banxyz.jpa.example.demo.service;

import cn.banxyz.jpa.example.common.AgeOverScopeException;
import cn.banxyz.jpa.example.common.InsertMIssException;
import cn.banxyz.jpa.example.demo.entity.User;
import cn.banxyz.jpa.example.demo.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ban_xz
 * @date 2020/07/03
 */


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public void insertInfo(User user) {
        if (user.getAge() > 18){
            throw new AgeOverScopeException("你的年龄为"+user.getAge()+"岁,已超过年龄的限制无法注册！");
        }
        User save = userDao.save(user);
        if(save == null ){
            throw new InsertMIssException("插入失败数据失败");
        }

    }
}
