package cn.banxyz.jpa.example.demo.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ban_xz
 * @date 2020/07/03
 */
@Entity
@Data
@Table(name = "t_user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Integer age;

    private String phone;

    private String address;

}
