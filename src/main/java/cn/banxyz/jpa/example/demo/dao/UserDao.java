package cn.banxyz.jpa.example.demo.dao;

import cn.banxyz.jpa.example.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ban_xz
 * @date 2020/07/03
 */

public interface UserDao extends JpaRepository<User, Integer>{


}
