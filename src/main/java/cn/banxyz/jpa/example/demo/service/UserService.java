package cn.banxyz.jpa.example.demo.service;

import cn.banxyz.jpa.example.demo.entity.User;


/**
 * @author ban_xz
 * @date 2020/07/03
 */

public interface UserService {

    void insertInfo(User user);
}
