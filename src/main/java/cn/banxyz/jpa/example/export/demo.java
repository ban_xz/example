package cn.banxyz.jpa.example.export;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @author ban_xz
 * @date 2020/08/28
 */

public class demo {


    @Test
    public void test1(HttpServletRequest req, HttpServletResponse resp){

        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        String fileName = "在线用户账号列表.xls";
        ServletUtil su = new ServletUtil(fileName, req, resp);
        su.poiExcelServlet();

        String[] heads = {"序号", "用户名", "登录时间", "登陆时长", "登陆IP", "状态", "属地"};
        String[] cols = {"id", "account", "loginTime", "landingTime", "loginIp", "statusName", "districtName"};
        new PoiExcelExport<>(heads, cols, list,su.getOut()).exportExcel();
    }


}
