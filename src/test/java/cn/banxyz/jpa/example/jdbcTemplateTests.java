package cn.banxyz.jpa.example;

import cn.banxyz.jpa.example.jdbc_template.entity.Province;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * @author ban_xz
 * @date 2020/07/27
 */
@SpringBootTest
public class jdbcTemplateTests {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void test1(){
        Integer provId = 12;
        List<Province> query = jdbcTemplate.query(
                "select *from province ",
                new Object[]{},
                new BeanPropertyRowMapper(Province.class));
        query.stream().forEach(System.out::println);

    }

    @Test
    public void test2(){
        List<Province> query = jdbcTemplate.query(
                "select *from province where id = :id  and prov_name = :provName",
                new Object[]{},
                new BeanPropertyRowMapper(Province.class));

    }


}
