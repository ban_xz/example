//package cn.banxyz.jpa.example;
//
//import cn.banxyz.jpa.example.demo.dao.CustomerDao;
//import cn.banxyz.jpa.example.demo.entity.Custer;
//import cn.banxyz.jpa.example.demo.entity.User;
//import cn.banxyz.jpa.example.demo.dao.UserDao;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.Rollback;
//
//import javax.annotation.Resource;
//import javax.transaction.Transactional;
//import java.io.IOException;
//
//@SpringBootTest
//class ExampleApplicationTests {
//
//    @Test
//    void contextLoads() {
//    }
//
//    @Resource
//    UserDao userDao;
//
//    @Autowired
//    CustomerDao customerDao;
//
//    @Test
//    @Transactional// 在测试类对于事务提交方式默认的是回滚。
//    @Rollback(false)//取消自动回滚
//    public void testInsertUsers() {
//        User user = new User();
//        user.setAge(18);
//        user.setName("Lisa");
//        user.setPhone("15874989654");
//        user.setAddress("莫斯科");
//        User save = userDao.save(user);
//        System.out.println(save);
//    }
//
//
//    @Test
//    public void JsonToEntity() throws IOException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        String jsonString = "{\"name\":\"张三\",\"sex\":\"男\",\"age\":25}";
//        User person = objectMapper.readValue(jsonString, User.class);
//        System.out.println(person);
//    }
//
//
//    @Test
//    public void tesJpaData() {
//        Custer custer = new Custer();
//        custer.setCustAddress("山东");
//        custer.setCustName("田七");
//        custer.setCustLevel("11");
//        custer.setCustPhone("1786236236232");
//        custer.setCustSource("qwe");
//        Custer save = customerDao.save(custer);
//        System.out.println(save);
//    }
//
//    @Test
//    public void delet() {
//        customerDao.deleteById(1L);
//    }
//
//    @Test
//    public void setUserDao() {
//        long count = customerDao.count();
//        System.out.println(count);
//    }
//
//
//    @Test
//    @Transactional
//    public void exits() {
//        boolean b = customerDao.existsById(2L);
//        System.out.println(b);
//        Custer one = customerDao.getOne(2L);
//        System.out.println(one);
//    }
//}
