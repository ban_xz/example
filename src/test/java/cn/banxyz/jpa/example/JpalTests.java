package cn.banxyz.jpa.example;

import cn.banxyz.jpa.example.demo.dao.CustomerDao;
import cn.banxyz.jpa.example.demo.entity.Custer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;


import java.util.Arrays;
import java.util.List;

/**
 * @author ban_xz
 * @date 2020/07/16
 */

@SpringBootTest
public class JpalTests {

    @Autowired
    CustomerDao customerDao;

    @Test
    public void setCustomerDao() {
        Custer jpal = customerDao.findJpal("王五",1L);
        System.out.println(jpal);

    }

    @Test
    @Transactional
    @Rollback(value = false)
    public void update() {
        customerDao.updateCustomer(1L,"花花");
    }

    @Test
    public void mysqlTest() {
        List<Object[]> mySql = customerDao.findMySql("");


        for (Object[] obj:mySql
        ) {
            System.out.println(Arrays.toString(obj));
        }
    }


    @Test
        public void test1() {
            List<Custer> list = customerDao.findByCustNameAndAndCustId("田七", 2L);
            list.stream().forEach(System.out::println);

    }


    @Test
    public void test3() {
        List<Custer> list = customerDao.findAll();
        list.stream().forEach(System.out::println);
        String ss = "0040FF000078000000004C4576000001";
        ss.length();
        System.out.println(ss.length());


    }

}
